import api from '@/api';

import axios from 'axios';

const state = () => ({
    profile: null,
    authToken: null,
});

const getters = {
    token: state => {
        if (state.authToken) {
            return state.authToken;
        }
        else if (localStorage.getItem('token')) {
            state.authToken = localStorage.getItem('token');
        }
        return null;
    },
    profile: state => state.profile
};

const actions = {
    async reg({ commit }, profile) {
        const response = await api.user.reg(profile);
        if (response) {
            commit('setProfile', response.data);
        }
        else {
            throw "invalid";
        }
    },
    async login({ commit }, { login, password }) {
        const response = await api.user.login(login, password);
        if (response) {
            commit('setProfile', response.data);
        }
        else {
            throw "invalid";
        }
    },
    logout({ commit }) {
        commit('unsetToken'); 
    },
    async me({ commit, state }) {
        const response = await api.user.check(state.authToken);
        if (response) {
            commit('setProfile', response.data);
        }
        else {
            throw "invalid";
        }
    }
};

const mutations = {
    setProfile(state, profile) {
        state.profile = profile;
        state.authToken = profile.token;
        localStorage.setItem('token', profile.token);
        axios.defaults.headers.common['Authorization'] = profile.token;
    },
    unsetToken(state) {
        state.authToken = null;
        state.profile = null;
        localStorage.removeItem('token');
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }

