import api from '@/api';

const state = () => ({
    all: []
});

const getters = {
    all: state => state.all
};

const actions = {
    async getAll({ commit }) {
        const response = await api.project.getAll();
        console.log(response);
        if (response) {
            commit('setAll', response.data);
        }
        else {
            throw "invalid";
        }
    }
};

const mutations = {
    setAll(state, projects) {
        state.all = projects;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }

