import apiCore from '@/api/apiCore';

const prefix = 'user/';

export default {
    async login(login, password) {
        return await apiCore.post(prefix + 'login', {
            name: login,
            password
        });
    },
    async reg(profile) {
        return await apiCore.post(prefix + 'registration', profile);
    },
    async check() {
        return await apiCore.post(prefix + 'check-login');
    }
};