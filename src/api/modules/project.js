import apiCore from '@/api/apiCore';

const prefix = 'project';

export default {
    async getAll() {
        return await apiCore.get(prefix + '/');
    },
};