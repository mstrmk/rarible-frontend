
import axios from 'axios';

axios.defaults.baseURL = 'https://defi.kwel.ru/api';

if (localStorage.getItem('token')) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');
}

export default {
    async get(method, params = {}) {
        let response;
        try {
            response = await axios.get(method, params);
            return response;
        } catch (e) {
            return false;
        }
    } ,
    async post(method, params = {}) {
        let response;
        try {
            response = await axios.post(method, params);
            return response;
        } catch (e) {
            return false;
        }
    }
};