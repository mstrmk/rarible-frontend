import user from './modules/user';
import project from './modules/project';

export default {
    user,
    project
};