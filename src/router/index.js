import { createRouter, createWebHashHistory } from "vue-router";

import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

// import Dashboard from "../views/Dashboard.vue";
// import Icons from "../views/Icons.vue";
// import Maps from "../views/Maps.vue";
// import Profile from "../views/UserProfile.vue";
import Tables from "../views/Tables.vue";

import Login from "../views/Rarible/Login.vue";
import Register from "../views/Rarible/Register.vue";
import Projects from "../views/Rarible/Projects.vue";

import auth from './middleware/auth';

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    beforeEnter: auth,
    children: [
      {
        path: "/",
        name: "projects",
        components: { default: Projects },
      },
      {
        path: "/dev",
        name: "dev",
        components: { default: Tables },
      },
    ],
  },

  {
    path: "/",
    redirect: "login",
    component: AuthLayout,
    children: [
      {
        path: "/login",
        name: "login",
        components: { default: Login },
      },
      {
        path: "/register",
        name: "register",
        components: { default: Register },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: "active",
  routes,
});

export default router;
