export default function auth(to, from, next) {
    if (to.name !== 'login' && to.name !== 'register' && !localStorage.getItem('token')){
        next({
           name: 'login'
        })
    }
    else {
        next()
    }
}